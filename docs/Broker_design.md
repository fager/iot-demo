# Aufbau der Topics im Broker

## Jedes IoT-Device hat

- 1 Temperatursensor
- 1 LED (zzgl. der onboard LED)
- 1 Taster

## Topics

wichtig:
- Jedes Device brauch eine eindeutige ID ("ChipID")

### Senden

- Temperatorwert
- LED-Schaltvorgang (an/aus)
- Taster-Event

### Empfangen

- LED-Befehl (an/aus)

### Aufbau der Topic auf dem Broker

- device/{device-name}/temp0
  - senden
  - Payload: Temperaturwert in °C
- device/{device-name}/led0 (ext)
  - senden
  - Payload: "on" oder "off"
  - wird nur geschickt, wenn sie sich ändert
- device/{device-name}/led1 (onboard)
  - senden
  - Payload: "on" oder "off"
  - wird nur geschickt, wenn sie sich ändert
- device/{device-name}/input0 (Taster)
  - senden
  - Payload: "pressed"
  - wird nur geschickt, wenn die Taste gedrückt wurde

- device/{device-name}/led0cmd
  - empfangen
  - Payload: "on" oder "off"
  - wird nur geschickt, wenn sie sich ändert
- device/{device-name}/led1cmd
  - empfangen
  - Payload: "on" oder "off"
  - wird nur geschickt, wenn sie sich ändert

- devreg/hello
  - Payload: json
  - Beispiel: {"ip": "192.0.2.1", "device-name": "{device-name}"}
  - wird beim Starten des Devices einmalig gesendet
- devreg/bye (optional)
  - Payload: json
  - Beispiel: {"ip": "192.0.2.1", "device-name": "{device-name}"}
  - Könnte vom MQTT-Server als Last-Will-Message gesendet werden

