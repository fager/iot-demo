#!/bin/bash


_archive_file="$(rpm --eval "%_sourcedir")/iot-1.0.0.tar.gz"
if test -f "${_archive_file}"
then
    echo "Entferne altes Source-Archiv"
    rm -f "${_archive_file}"
fi

echo "Erstelle neues Source-Archiv"
git archive --prefix iot-1.0.0/ --format=tar.gz HEAD -o "${_archive_file}"

echo "Erstelle RPM"
rpmbuild -ba iot.spec

