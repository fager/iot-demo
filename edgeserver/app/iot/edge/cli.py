
import argparse
import sys
import logging
from dotenv import load_dotenv
import iot.edge.db
import iot.edge.device as cli_device
from iot import __version__
from iot.edge import setup_logger

load_dotenv()

def create_argparser():
    """Create argparser for this cli"""

    parent_parser = argparse.ArgumentParser(
        description="IoT Edge Demo",
        allow_abbrev=True
    )
    parent_parser.add_argument(
        '-V',
        '--version',
        action="version",
        version=f"{__version__}"
    )
    parent_parser.add_argument(
        '-v',
        '--verbose',
        action='store_true'
    )

    subparsers = parent_parser.add_subparsers(help="Resource")

    cli_device.create_parser(subparsers)
    
    return parent_parser
    
def main():

    parser = create_argparser()
    args = parser.parse_args()
    log = setup_logger('iot-demo', args.verbose)

    db = iot.edge.db.EdgeDatabase()
    db.init_db()

    if not hasattr(args, 'func'):
        log.error("No action provided")
        parser.print_help()
        sys.exit(1)
    
    result = False
    try:
        result = args.func(db, log, args)
    except Exception as e:
        log.debug(e, exc_info=True)
        result = False
    
    if result is False:
        sys.exit(1)

if __name__ == '__main__':
    main()
