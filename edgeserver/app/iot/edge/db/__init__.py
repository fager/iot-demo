#!/usr/bin/env python3

# Importing Sqlite3 Module
import sqlite3
import logging
import os

class EdgeDatabase():

    _uri = None
    _log = None

    conn = None

    def __init__(self, uri=None, log=None):
        self._uri = uri
        self._log = log
        
        if self._log is None:
            self._log = logging.getLogger()

    def connect_db(self):
        
        if not self._uri:
            if 'DB_URI' in os.environ:
                self._uri = os.environ.get('DB_URI')
            else:
                self._uri = "file:data.db"

        if not self.conn:
            self.conn = sqlite3.connect(self._uri, uri=True)
            self.conn.row_factory = sqlite3.Row

    def init_db(self):
        try:
            self.connect_db()

            # Getting all tables from sqlite_master
            sql_query = """SELECT name FROM sqlite_master
            WHERE type='table';"""
         
            # Creating cursor object using connection object
            cursor = self.conn.cursor()
             
            # executing our sql query
            cursor.execute(sql_query)
            self._log.debug("List of tables\n")
             
            # printing all tables list
            all_tables = cursor.fetchall()
            for table in all_tables:
                self._log.debug(f"- {table[0]}")
            SQL = """CREATE TABLE IF NOT EXISTS devices
                (device_id VARCHAR(60), ip VARCHAR(15),
                led0 TINY INT,
                temp0 FLOAT,
                created DATETIME,
                modified DATETIME)
            """
            cursor.execute(SQL)

        except sqlite3.Error as error:
            self._log.error(error, exc_info=True)
             
        finally:
           
            # Inside Finally Block, If connection is
            # open, we need to close it
            if self.conn:
                 
                # using close() method, we will close
                # the connection
                self.conn.close()
                self.conn = None
                 
                # After closing connection object, we
                # will print "the sqlite connection is
                # closed"
                self._log.debug("the sqlite connection is closed")

    

