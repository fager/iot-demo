
import argparse
import sqlite3
from iot.edge.db import EdgeDatabase

class DeviceDB():
    
    _db: EdgeDatabase = None
    
    def __init__(self, db: EdgeDatabase):
        self._db = db


    def get_device(self, device_id):
        ret = None
        try:
            self._db.connect_db()
            SQL1 = """SELECT
                device_id, ip, led0, temp0, created, modified
                FROM devices
                WHERE device_id=:device_id
            """
            cur = self._db.conn.execute(SQL1, {'device_id': device_id})
            row = cur.fetchone()
            if row != None:
                ret = dict(row)
            cur.close()
        except sqlite3.Error as error:
            print("Failed to execute the above query", error)
        return ret

    def list_devices(self):
        ret = []
        try:
            self._db.connect_db()
            SQL1 = """SELECT
                device_id
                FROM devices
            """
            cur = self._db.conn.execute(SQL1)
            for row in cur:
                ret.append(row['device_id'])
            cur.close()
        except sqlite3.Error as error:
            print("Failed to execute the above query", error)
        return ret

    def devreg_helo(self, device_id, ip):
        try:
            self._db.connect_db()
            device = self.get_device(device_id)
            if device:
                # Device found
                SQL2 = """UPDATE devices
                    SET ip=:ip,modified=datetime()
                    WHERE device_id=:device_id
                """
                cur2 = self._db.conn.execute(SQL2, {'device_id': device_id, 'ip': ip})
                self._db.conn.commit()
                cur2.close()
            else:
                # device not found
                SQL2 = """INSERT INTO devices
                    (device_id, ip, created, modified)
                    VALUES
                    (:device_id, :ip, datetime(), datetime())
                """
                cur2 = self._db.conn.execute(SQL2, {'device_id': device_id, 'ip': ip})
                self._db.conn.commit()
                cur2.close()

        except sqlite3.Error as error:
            print("Failed to execute the above query", error)

    def update_led0(self, device_id, led0: int):
        ret = False
        try:
            self._db.connect_db()
            SQL2 = """UPDATE devices
                SET led0=:led0,modified=datetime()
                WHERE device_id=:device_id
            """
            cur2 = self._db.conn.execute(SQL2, {'device_id': device_id, 'led0': led0})
            self._db.conn.commit()
            cur2.close()
            ret = True
        except sqlite3.Error as error:
            print("Failed to execute the above query", error)
        return ret

    def update_temp0(self, device_id, temp0: float):
        ret = False
        try:
            self._db.connect_db()
            SQL2 = """UPDATE devices
                SET temp0=:temp0,modified=datetime()
                WHERE device_id=:device_id
            """
            cur2 = self._db.conn.execute(SQL2, {'device_id': device_id, 'temp0': temp0})
            self._db.conn.commit()
            cur2.close()
            ret = True
        except sqlite3.Error as error:
            print("Failed to execute the above query", error)
        return ret

def device_get(db: EdgeDatabase, log, args):
    ddb = DeviceDB(db)
    dev = ddb.get_device(args.device)
    print("Device-Info:")
    for k in dev.keys():
        print(f"  {k:20}  {dev[k]}")
    
    return True

def device_list(db: EdgeDatabase, log, args):
    ddb = DeviceDB(db)
    dev_list = ddb.list_devices()
    print("Devices:")
    for dev in dev_list:
        print(f"- {dev}")

    return True
    
def create_parser(subparsers):
    device_parser = subparsers.add_parser('device', help="Device commands")
    
    subcommands = device_parser.add_subparsers(help="action")
    
    list_parser = subcommands.add_parser("list", help="list")
    list_parser.set_defaults(func=device_list)
    
    get_parser = subcommands.add_parser("get", help="get")
    get_parser.add_argument("device", help="Device name")
    get_parser.set_defaults(func=device_get)
    
    del_parser = subcommands.add_parser("delete", help="delete")
    del_parser.add_argument("device", help="Device name")
    
