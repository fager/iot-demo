#!/usr/bin/env python3

from paho.mqtt import client as mqtt_client
from dotenv import load_dotenv
import random
import time
import logging
import os
import iot.edge.db
import iot.edge.device
import json
import argparse
from iot import __version__
from iot.edge import setup_logger

FIRST_RECONNECT_DELAY = 1
RECONNECT_RATE = 2
MAX_RECONNECT_COUNT = 12
MAX_RECONNECT_DELAY = 60

load_dotenv()

client_id = f"iot-server-{random.randint(0,1000)}"

def create_argparser():
    """Create argparser for this server"""

    parent_parser = argparse.ArgumentParser(
        description="IoT Edge Demo",
        allow_abbrev=True
    )
    parent_parser.add_argument(
        '-V',
        '--version',
        action="version",
        version=f"{__version__}"
    )
    parent_parser.add_argument(
        '-v',
        '--verbose',
        default=False,
        action='store_true'
    )

    #subparsers = parent_parser.add_subparsers(help="Resource")
    
    return parent_parser

def connect_mqtt() ->mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    if 'MQTT_USERNAME' in os.environ:
        print(f"User: {os.environ.get('MQTT_USERNAME')}")
        client.username_pw_set(os.environ.get('MQTT_USERNAME'), os.environ.get('MQTT_PASSWORD'))
    client.on_connect = on_connect
    client.tls_set(ca_certs=os.environ.get('MQTT_CAFILE'))
    client.connect(os.environ.get('MQTT_SERVER'), int(os.environ.get('MQTT_PORT')))
    return client

def on_disconnect(client, userdata, rc):
    logging.info("Disconnected with result code: %s", rc)
    reconnect_count, reconnect_delay = 0, FIRST_RECONNECT_DELAY
    while reconnect_count < MAX_RECONNECT_COUNT:
        logging.info("Reconnecting in %d seconds...", reconnect_delay)
        time.sleep(reconnect_delay)

        try:
            client.reconnect()
            logging.info("Reconnected successfully!")
            return
        except Exception as err:
            logging.error("%s. Reconnect failed. Retrying...", err)

        reconnect_delay *= RECONNECT_RATE
        reconnect_delay = min(reconnect_delay, MAX_RECONNECT_DELAY)
        reconnect_count += 1
    logging.info("Reconnect failed after %s attempts. Exiting...", reconnect_count)

def is_temp_too_high(device, temp):
    limit_s = os.getenv("TEMP_LIMIT", default="27")
    limit = float(limit_s)
    if float(temp) > limit:
        return True
    else:
        return False

def subscribe(client: mqtt_client, db: iot.edge.db.EdgeDatabase, log: logging.Logger):
    
    def on_devreg(client, userdata, msg):
        log.info(f"devreg: Received `{msg.payload.decode()}` from `{msg.topic}` topic")

        j = json.loads(msg.payload.decode())
        db.devreg_helo(j['device-name'], j['ip'])
        

    def on_message(client, userdata, msg):
        log.info(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

    def on_temp(client, userdata, msg):
        topic = msg.topic
        _, device, idx = topic.split('/')
        temp0 = msg.payload.decode()
        dev = db.get_device(device)
        if dev:
            if dev['temp0'] != float(temp0):
                log.info(f"temp: Device {device} temp0 changed to {temp0}")
                db.update_temp0(device, float(temp0))
                topic_led0cmd = f"device/{device}/led0cmd"
                if is_temp_too_high(device, temp0):
                    client.publish(topic_led0cmd, "on")
                else:
                    client.publish(topic_led0cmd, "off")
        else:
            log.info(f"temp: Device {device} sent {idx}={temp0}")

    def on_led(client, userdata, msg):
        topic = msg.topic
        _, device, idx = topic.split('/')
        cmd = msg.payload.decode()
        log.info(f"led: Device {device} switched {idx} to {cmd}")
        led0 = 0
        on = ('on', '1', 'ON')
        if cmd in on:
            led0 = 1
        db.update_led0(device, led0)

    def on_input(client, userdata, msg):
        topic = msg.topic
        _, device, idx = topic.split('/')
        cmd = msg.payload.decode()
        log.info(f"input: Device {device} input {idx} sent {cmd}")

    topic = "device/#"
    client.subscribe("device/+/temp0")
    client.subscribe("device/+/led0")
    client.subscribe("device/+/input0")
    client.subscribe("devreg/hello")
    client.subscribe("devreg/bye")

    client.message_callback_add("devreg/hello", on_devreg)
    client.message_callback_add("device/+/temp0", on_temp)
    client.message_callback_add("device/+/led0", on_led)
    client.message_callback_add("device/+/input0", on_input)

def main():
    
    
    args_parser = create_argparser()
    args = args_parser.parse_args()
    
    log = setup_logger('iot-server', args.verbose)
    log.info("Starting server")
    
    db = iot.edge.db.EdgeDatabase(log=log)
    db.init_db()
    devices = iot.edge.device.DeviceDB(db)
    client = connect_mqtt()
    client.on_disconnect = on_disconnect
    subscribe(client, devices, log)
    client.loop_forever()
    

if __name__ == '__main__':
    main()
