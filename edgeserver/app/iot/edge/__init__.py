
import logging
import sys

def setup_logger(name: str, verbose: bool = False):
    
    class StdErrFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno in (logging.ERROR,)
    
    class StdOutFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno not in (logging.ERROR,)

    root = logging.getLogger()
    log = logging.getLogger(name)
    
    if verbose:
        log.setLevel(logging.DEBUG)
        log_format = '%(levelname)s: %(message)s'
    else:
        log.setLevel(logging.INFO)
        log_format = '%(message)s'
    
    log_handler = logging.StreamHandler(sys.stdout)
    log_handler.setFormatter(logging.Formatter(log_format))
    log_handler.addFilter(StdOutFilter())
    
    root.addHandler(log_handler)
    
    log_handler_err = logging.StreamHandler(sys.stderr)
    log_handler_err.setLevel(logging.ERROR)
    log_handler_err.addFilter(StdErrFilter())
    log_handler_err.setFormatter(logging.Formatter(log_format))
    log.addHandler(log_handler_err)
    
    return log