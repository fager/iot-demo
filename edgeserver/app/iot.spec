%global pypi_name iot
%global pypi_version 1.0.0

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        3%{?dist}
Summary:        IoT Demo

License:        GPLv3
Source0:        %{pypi_name}-%{pypi_version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
IoT Demo Client and Server

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(requests)
%description -n python3-%{pypi_name}
IoT Demo Client and Server


%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.adoc
%attr(4755, root, root) %{_bindir}/iot-edge
%attr(4755, root, root) %{_bindir}/iot-edge-server
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Wed Sep 13 2023 Frank Agerholm <frank@agerholm.de> - 1.0.0
- New upstream release 1.0.0

