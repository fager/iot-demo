= IoT Server Anwendung für die Edge-Locations

== Starten des Servers

[source,bash]
----
python3 -m iot.edge.server
----

== Client

[source,bash]
----
python3 -m iot.edge.cli --help
----