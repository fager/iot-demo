Name:       iot-info
Version:    1.0.0
Release:    2
Summary:    A IoT Demo app
License:    FIXME
BuildArch:      noarch
Requires:    greenboot

%description
This Pakage was build with a single file.

%prep
echo "Starting preparations for build section ..."

%build

printenv | sort 

cat > iot-info.sh <<EOF
#!/usr/bin/bash

# Version: ${RPM_PACKAGE_VERSION}

while [ "\${#}" -gt 0 ]
do
    _key="\${1}"
    shift
    case "\${_key}" in
        -v|--version)
            echo "${RPM_PACKAGE_VERSION}"
            exit 0
        ;;
    esac
done

echo "IoT Info"

EOF

%install
mkdir -p %{buildroot}/usr/bin/
install -m 755 iot-info.sh %{buildroot}/usr/bin/iot-info.sh

%files
%defattr(-,root,root,775)
/usr/bin/iot-info.sh

%changelog
* Sun Sep 10 2023 Frank Agerholm <frank@agerholm.de> - 1.0.0-2
- New Release 1.0.0


