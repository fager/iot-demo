Name:       iot-info
Version:    1.1.0
Release:    2
Summary:    A IoT Info app
License:    FIXME
BuildArch:      noarch
Requires:       bash

%description
This Pakage was build with a single file.

%prep
echo "Starting preparations for build section ..."

%build

printenv | sort 

cat > iot-info.sh <<EOF
#!/usr/bin/bash

# Version: ${RPM_PACKAGE_VERSION}

while [ "\${#}" -gt 0 ]
do
    _key="\${1}"
    shift
    case "\${_key}" in
        -v|--version)
            echo "${RPM_PACKAGE_VERSION}"
            exit 0
        ;;
    esac
done

echo "IoT Info"

EOF

cat > greenboot-required-fail.sh <<EOF
#!/usr/bin/bash

# Version: ${RPM_PACKAGE_VERSION}

echo "Failure during iot-demo greenboot check"
exit 1

EOF

%install
ls -lah
mkdir -p %{buildroot}/usr/bin/
mkdir -p %{buildroot}/etc/greenboot/check/required.d
install -m 755 iot-info.sh %{buildroot}/usr/bin/iot-info.sh
install -m 755 greenboot-required-fail.sh %{buildroot}/etc/greenboot/check/required.d/iot-demo.sh

%files
%defattr(-,root,root,755)
/usr/bin/iot-info.sh
/etc/greenboot/check/required.d/iot-demo.sh

%changelog
* Sun Sep 10 2023 Frank Agerholm <frank@agerholm.de> - 1.1.0-2
- Release 1.1.0
- greenboot checks

