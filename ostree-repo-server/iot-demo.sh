#!/bin/bash

source lib.sh

if ! setup
then
    echo "ERROR: setup failure"
    exit 1
fi

_opt_cmd="usage"
_opt_release=""
_opt_build_version=""
_opt_files=false


while [ "$#" -gt 0 ]
do
    _key="${1}"
    shift
    case "${_key}" in
        --build)
            _opt_cmd="build"
            if [ "$#" -gt 0 ]
            then
                _opt_build_version="${1}"
                shift
            else
                _opt_build_version="1.0.0"
            fi
        ;;
        --build-all)
            _opt_cmd="build-all"
        ;;
        --build-installer)
            _opt_cmd="build-installer"
        ;;
        --purge)
            _opt_cmd="purge"
        ;;
        --selinux)
            _opt_cmd="selinux"
            repo_selinux
            exit 0
        ;;
        --status)
            _opt_cmd="status"
        ;;
        --release)
            _opt_cmd="release"
            _opt_release="${1}"
            shift
        ;;
        --files)
            _opt_files=true
        ;;
        --import-assets)
            _opt_cmd="importassets"
            _opt_release="${1}"
            shift
        ;;
    esac
done

case "${_opt_cmd}" in
    status)
        cmd_status
    ;;
    release)
        cmd_release --name "edge" --version "${_opt_release}"
    ;;
    build)
        cmd_build --name "edge" --version "${_opt_build_version}"
    ;;
    build-all)
        cmd_build --name "edge" --version "1.0.0"
        cmd_build --name "edge" --version "1.1.0"
        cmd_build --name "edge" --version "1.2.0"
    ;;
    build-installer)
        cmd_build_installer
    ;;
    purge)
        if $_opt_files
        then
            cmd_purge --files
        else
            cmd_purge
        fi
    ;;
    importassets)
        cmd_importassets "${_opt_release}"
    ;;
esac

exit 0

