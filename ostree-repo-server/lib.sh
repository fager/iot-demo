
if test -f .env
then
    # Load .env file
    . .env
fi

. /etc/os-release

ID="${ID:-unknown}"
VERSION_ID="${VERSION_ID:-unknown}"

OSTREE_URL="${OSTREE_URL:-http://localhost/ostree}"
export OSTREE_URL

OSTREE_REPO="${OSTREE_REPO:-/var/www/html/ostree/}"
export OSTREE_REPO

# Name des ostree refs fuer die produktiven Systeme
BUILD_RELEASE_REF="${ID}/${VERSION_ID}/$(uname -m)/edge"
export BUILD_RELEASE_REF

# Name des ostree refs fuer den neusten Build
BUILD_REF_LATEST="build/${ID}/${VERSION_ID}/$(uname -m)/latest/edge"
export BUILD_REF_LATEST

toml_name() {
    tomcli-get - name
}

toml_version() {
    tomcli-get - version
}

blueprint_exists() {
    composer-cli blueprints list | grep --quiet -e "^${1}$"
    return "$?"
}

msg_task() {
    local _msg
    _msg="${*}"
    printf "[%s] %s\n" "$(date +%H:%M:%S)" "${_msg}" 
}

msg_status() {
    local _msg
    _msg="${*}"
    printf "[%s] ... %s\n" "$(date +%H:%M:%S)" "${_msg}" 
}

msg_error() {
    local _msg
    _msg="${*}"
    printf "[%s] ERROR: %s\n" "$(date +%H:%M:%S)" "${_msg}" 
}

msg_kv() {
    local _key
    local _val
    _key="${1:-n.a.}"
    _val="${2:-n.a.}"
    printf "[%s]     %-25s  : %s\n" "$(date +%H:%M:%S)" "${_key}" "${_val}" 
}

buildref_split() {
    local _ref
    local _output
    local _rc
    _rc=0
    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            -o|--output)
                _output="${1}"
                shift
            ;;
            *)
                _ref="${_key}"
            ;;
        esac
    done

    local _refparts
    _refparts=()
    IFS="/" read -r -a _refparts <<< "${_ref}"

    if [ "${#_refparts[@]}" -ne 6 ]
    then
        _rc=1
    fi

    if [ "${_rc}" = 0 ]
    then
        case "${_output}" in
            build-date)
                echo "${_refparts[4]}"
            ;;
            id)
                echo "${_refparts[1]}"
            ;;
            version-id)
                echo "${_refparts[2]}"
            ;;
            machine)
                echo "${_refparts[3]}"
            ;;
            *)
                _rc=1
            ;;
        esac
    fi
    return "${_rc}"
}

repo_selinux() {
    local _selinux_mode
    _selinux_mode="$(getenforce)"

    if [ "${_selinux_mode}" = "Enforcing" ]
    then
        msg_task "SELinux relabeling (Please enter sudo password)"
        if sudo /usr/sbin/restorecon -R "${OSTREE_REPO}"
        then
            msg_status "finished"
        fi
    fi
}

repo_ref_exists() {
    local _ref
    local _rc
    _rc=0
    _ref="${1}"
    if ostree refs | grep --quiet -e "^${_ref}$"
    then
        _rc=0
    else
        _rc=1
    fi
    #echo "Searching ref ${_ref} = ${_rc}"
    return "${_rc}"
}

version_is_valid() {
    local _name
    local _version
    local _rc
    _rc=0

    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --name)
                _name="${1}"
                shift
            ;;
            --version)
                _version="${1}"
                shift
            ;;
        esac
    done

    if [ -z "${_name}" ] && [ -z "${_version}" ]
    then
        _rc=1
    else
        if ! [ -f "${_name}-${_version}.toml" ]
        then
            _rc=2
        fi
    fi
    return "${_rc}"
}

get_build_var() {
    local _version
    local _name
    local _output
    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --version)
                _version="${1}"
                shift
            ;;
            --name)
                _name="${1}"
                shift
            ;;
            -o|--output)
                _output="${1}"
                shift
            ;;
        esac
    done

    if ! version_is_valid --name "${_name}" --version "${_version}"
    then
        return 1
    fi

    if [ -z "${BUILD_DATE}" ]
    then
        # Datumscode fuer den Ref-Namen
        # variable wird exportiert, damit sie
        # beim gesamten Script-Durchlauf den
        # gleichen Wert zurueck gibt
        #
        # Da der Wert auch bei anderen Output
        # benoetigt wird, wird er immer generiert,
        # wenn er nicht vorhanden ist
        BUILD_DATE="$(date +%Y%m%d.%H%M%S)"
        export BUILD_DATE
    fi

    case "${_output}" in
        build-version)
            echo "${_version}"
        ;;
        build-date)
            echo "${BUILD_DATE}"
        ;;
        build-ref)
            # ostree-ref, unter dem der commit gespeichert wird
            echo "build/${ID}/${VERSION_ID}/$(uname -m)/${BUILD_DATE}/${_name}-${_version}"
        ;;
    esac
}

repo_ref_get_latest() {
    local _search
    _search="${1}"

    ostree refs | grep -e "${_search}$" | tail -1
}

get_compose_id() {
    _name="$1"
    _version="$2"
    _ID="$(composer-cli compose list | awk "\$2 ~ /FINISHED/ &&\$3 ~ /${_name}/ && \$4 ~ /${_version}/ { print \$1; }")"
    echo $_ID
}

get_compose_status() {
    _id="$1"
    composer-cli compose status | grep -e "^${_id}" | awk '{print $2}'
    return "$?"
}

wait_for_compose_job() {
    _id="$1"
    _run=true
    msg_status "Waiting for job ${_id} to finish:"
    while $_run
    do
        
        _status="$(get_compose_status "${_id}")"
        if [[ "${_status}" = "RUNNING" ]]
        then
            printf "."
            sleep 5
        else
            _run=false
        fi
    done
    printf "\n"
}

db_add() {
    local _line
    _line="${*}"
    echo "${_line}" >> .db
}

db_get() {
    local _rc
    local _type
    _rc=0

    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --type)
                _type="${1}"
                shift
            ;;
        esac
    done

    if [ -f ".db" ]
    then
        grep -e "^${_type}" .db
    else
        _rc=1
    fi
    return "${_rc}"
}

update_blueprint() {
    local _name
    local _version
    local _rc
    _rc=0

    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --name)
                _name="${1}"
                shift
            ;;
            --version)
                _version="${1}"
                shift
            ;;
        esac
    done

    if ! version_is_valid --name "${_name}" --version "${_version}"
    then
        return 1
    fi

    msg_task "Upload blueprint"

    local _current_version
    if [ -n "${_name}" ]
    then
        if blueprint_exists "${_name}"
        then
            _current_version="$(composer-cli blueprints show "${_name}" | tomcli-get - version)"
        fi
    fi

    if [[ "${_current_version}" != "${_version}" ]]
    then
        msg_status "Updating ${_name} blueprint to version ${_version}"
        if ! composer-cli blueprints push "${_name}-${_version}.toml"
        then
            _rc=3
        fi
    else
        msg_status "blueprint allready in version ${_version}"
    fi
    return "${_rc}"
}

ostree_import_commit() {

    local _build_id
    local _build_ref
    while [ "${#}" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --build-id)
                _build_id="${1}"
                shift
            ;;
            --build-ref)
                _build_ref="${1}"
            ;;
        esac
    done

    msg_task "Importing ${_build_id} (${_build_ref}) into ostree repo"
    if ! test -f "./assets/${_build_id}.tar"
    then
        msg_error "Result tar for ${_build_id} not found"
        return 1
    fi

    if ! test -f "${_build_id}-commit.tar"
    then
        msg_status "extracting commit.tar from results"
        tar -xf "./assets/${_build_id}.tar" "${_build_id}-commit.tar"
    fi

    test -d ./tmp || mkdir tmp

    if ! test -d "./tmp/${_build_id}"
    then
        msg_status "extracting commit.tar into tmp-dir"
        mkdir "./tmp/${_build_id}"
        if tar -xf "${_build_id}-commit.tar" -C "./tmp/${_build_id}"
        then
            rm -f "${_build_id}-commit.tar"
        fi
    fi

    msg_status "Importing commit from tmp-dir into ostree repo"

    # ref am Ende ist die ref, die im Commit mit angegeben ist
    # Beim import-Pfad muss das "/repo" am Ende mit ran, weil
    # im commit.tar noch eine composer.json und die zusaetzliche
    # Verzeichnisebene repo/ enthalten ist.
    # Ein direkter Import des tar-files ohne vorherigen Entpacken geht nicht
    # temp: Am Ende war noch "f38/$(uname -m)/edge", tmp entfernt, weil build ein ref enthalten sollte
    if ostree pull-local "./tmp/${_build_id}/repo"
    then
        if repo_ref_exists "${BUILD_REF_LATEST}"
        then
            msg_status "Deleting old ref ${BUILD_REF_LATEST}"
            ostree refs --delete "${BUILD_REF_LATEST}"
        fi 

        msg_status "Creating ostree-ref ${BUILD_REF_LATEST}"
        ostree refs "${_build_ref}" --create "${BUILD_REF_LATEST}"

        # wenn import Erfolgreich war, tmp-dir gleich wieder loeschen
        msg_status "deleting tmp-dir"
        rm -Rf "./tmp/${_build_id}"

    else
        msg_error "Error during import into ostree repo"
        exit 1
    fi

    repo_selinux

}

#
# Sub-Commands
#


setup() {
    local _rc
    _rc=0
    if ! test -d "${OSTREE_REPO}"
    then
        echo "ostree-repo directory not found"
        echo "please create $OSTREE_REPO"
        exit 1
    fi

    if ! test -d "./assets"
    then
        mkdir ./assets
    fi

    if ! test -d "${OSTREE_REPO}" || ! test -f "${OSTREE_REPO}/config"
    then
        ostree init --repo "$OSTREE_REPO" --mode=archive
    fi
    return "${_rc}"
}

cmd_purge() {
    local _opt_files
    _opt_files=false
    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --files)
                _opt_files=true
            ;;
        esac
    done

    msg_task "Purging environment"

    msg_task "purging composes"

    while read -r _id
    do
        if [[ "${_id}" != "ID" ]]
        then
            msg_status "compose ${_id}"
            if [[ "$(get_compose_status "${_id}")" = "RUNNING" ]]
            then
                composer-cli compose cancel "${_id}"
            fi
            composer-cli compose delete "${_id}"

            if $_opt_files
            then
                msg_status "Deleting downloaded files"
                rm -fv ${_id}*
            fi
        fi
    done < <(composer-cli compose list | awk '{print $1}')

    msg_task "purging blueprints"
    if blueprint_exists "edge"
    then
        composer-cli blueprints delete edge
    fi

    if repo_ref_exists "${BUILD_REF_LATEST}"
    then
        # Die ostree ref zum letzten Build entfernen
        msg_task "Deleting latest build ref"
        ostree refs --delete "${BUILD_REF_LATEST}"
    fi

    msg_task "Cleaning ostree repo"
    rm -Rf ${OSTREE_REPO}/* ${OSTREE_REPO}/.lock
}

cmd_release() {
    local _name
    local _version
    local _rc
    _rc=0

    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --name)
                _name="${1}"
                shift
            ;;
            --version)
                _version="${1}"
                shift
            ;;
        esac
    done

    if ! version_is_valid --name "${_name}" --version "${_version}"
    then
        return 1
    fi

    msg_task "Releasing version ${_version}"
    local _ref
    _ref="$(repo_ref_get_latest "${_name}-${_version}")"

    if [[ -z "${_ref}" ]]
    then
        msg_error "No Build for ${_name}-${_version} found"
        _rc=1
    else
        msg_kv "latest ref" "${_ref}"

        local _build_date
        _build_date="$(buildref_split --output build-date "${_ref}")"
        msg_kv "Build date" "${_build_date}"
        
        local _version_id
        _version_id="$(buildref_split --output version-id "${_ref}")"
        msg_kv "Build version id" "${_version_id}"

        local _id
        _id="$(buildref_split --output id "${_ref}")"
        msg_kv "Build id" "${_id}"

        local _machine
        _machine="$(buildref_split --output machine "${_ref}")"
        msg_kv "Machine" "${_machine}"

        local _commit_id
        _commit_id="$(ostree rev-parse "${_ref}")"
        msg_kv "Commit-id" "${_commit_id}"

        local _release_ref
        _release_ref="${_id}/${_version_id}/${_machine}/${_name}"
        msg_kv "Release-ref" "${_release_ref}"

        msg_status "Creating release commiti ${_release_ref}"
        ostree commit -b "${_release_ref}" -s "Release ${_version}" --add-metadata-string="version=${_version_id}.${_build_date}" --tree="ref=${_commit_id}"

        repo_selinux
    fi
    return "${_rc}"
}

cmd_status() {
    msg_task "Compuse Status"
    composer-cli compose status
}

cmd_build_installer() {

    local _name
    _name="edge-installer"
    local _version
    _version="1.0.0"

    update_blueprint --name "${_name}" --version "${_version}"

    msg_task "Create installer raw image"

    local _json
    _json="$(composer-cli compose start-ostree --json --ref "${ID}/${VERSION_ID}/$(uname -m)/edge" --url "${OSTREE_URL}" "${_name}" iot-raw-image)"
    _compose_start_rc="${?}"
    if [ "${_compose_start_rc}" -ne 0 ]
    then
        local _err_msg
        _err_msg="$(echo "${_json}" | jq --raw-output '.[0].body.errors[0].msg')"
        msg_error "${_err_msg}"
    fi
    BUILD_ID="$(echo "${_json}" | jq --raw-output '.[0].body.build_id')"
    msg_kv "Build id" "${BUILD_ID}"

    wait_for_compose_job "${BUILD_ID}"

    BUILD_STATUS="$(get_compose_status "${BUILD_ID}")"

    if [[ "${BUILD_STATUS}" != "FINISHED" ]]
    then
        msg_error "Build status ${BUILD_STATUS}. Please check logs"
        exit 1
    else
        db_add "iot-raw-image;${_name};${BUILD_ID};${_version};${_build_date};${_build_ref}"
        msg_status "Downloading results"
        if ! test -f "${BUILD_ID}.tar"
        then
            composer-cli compose results "$BUILD_ID"
        fi
        msg_status "Uncompressing results"
        if [ -f "${BUILD_ID}.tar" ]
        then
            tar xf "${BUILD_ID}.tar"
        else
            msg_error "Result tar for ${BUILD_ID} not found"
            exit 1
        fi
    fi
}

cmd_build() {
    local _name
    local _version
    local _rc
    local _opt_update
    _opt_update=false
    _rc=0

    while [ "$#" -gt 0 ]
    do
        local _key
        _key="${1}"
        shift
        case "${_key}" in
            --name)
                _name="${1}"
                shift
            ;;
            --version)
                _version="${1}"
                shift
            ;;
            --update)
                _opt_update=true
            ;;
        esac
    done

    if ! version_is_valid --name "${_name}" --version "${_version}"
    then
        return 1
    fi

    update_blueprint --name "${_name}" --version "${_version}"

    _build_ref="$(get_build_var --name "${_name}" --version "${_version}" --output "build-ref")"
    _build_date="$(get_build_var --name "${_name}" --version "${_version}" --output "build-date")"

    msg_task "Building $BUILD_VER commit for edge"
    msg_kv "Repo" "${OSTREE_REPO}"
    msg_kv "Repo-URL" "${OSTREE_URL}"
    msg_kv "Build ref" "${_build_ref}"

    msg_task "Starting compose for blueprint edge"
    _start_ostree_opt=(--json --ref "${_build_ref}")
    if $_opt_update
    then
        msg_status "creating Update from ${OSTREE_URL} based on ${BUILD_REF_LATEST}"
        _start_ostree_opt+=(--url "${OSTREE_URL}" --parent "${BUILD_REF_LATEST}")
    fi
    msg_status "starting build job"
    local _compose_start_rc
    _json="$(composer-cli compose start-ostree "${_start_ostree_opt[@]}" "${_name}" iot-commit)"
    _compose_start_rc="${?}"
    if [ "${_compose_start_rc}" -ne 0 ]
    then
        local _err_msg
        _err_msg="$(echo "${_json}" | jq --raw-output '.[0].body.errors[0].msg')"
        msg_error "${_err_msg}"
    fi
    # Example output:
    # [
    #     {
    #         "method": "POST",
    #         "path": "/compose",
    #         "status": 200,
    #         "body": {
    #             "build_id": "811d1e3f-bb72-46cf-a4b0-84283c3c473f",
    #             "status": true,
    #             "warnings": null
    #         }
    #     }
    # ]

    BUILD_ID="$(echo "${_json}" | jq --raw-output '.[0].body.build_id')"
    msg_kv "Build id" "${BUILD_ID}"

    wait_for_compose_job "${BUILD_ID}"

    BUILD_STATUS="$(get_compose_status "${BUILD_ID}")"

    if [[ "${BUILD_STATUS}" != "FINISHED" ]]
    then
        msg_error "Build status ${BUILD_STATUS}. Please check logs"
        exit 1
    else
        db_add "iot-commit;${_name};${BUILD_ID};${_version};${_build_date};${_build_ref}"
        msg_status "Downloading results"
        if ! test -f "${BUILD_ID}.tar"
        then
            composer-cli compose results "$BUILD_ID"
        fi
        mv "${BUILD_ID}.tar" ./assets/
        ostree_import_commit --build-id "${BUILD_ID}" --build-ref "${_build_ref}"
    fi
}

cmd_importassets() {
    local _profile_id
    _profile_id="${1}"

    if ! test -f "./assets/${_profile_id}.csv"
    then
        msg_error "Asset profile ${_profile_id} not found"
        return 1
    fi

    local _type
    local _os
    local _build_id
    local _version
    local _build_date
    local _ref
    while IFS=";" read -r _type _os _build_id _version _build_date _ref
    do
        ostree_import_commit --build-id "${_build_id}" --build-ref "${_ref}"
    done < "./assets/${_profile_id}.csv"

}
